import React, {Component} from 'react';
import {View, Modal} from 'react-native';

import Switch from './Switch'

type Props = {};
export default class App extends Component<Props> {
  state = {
    visible: false,
    value: false
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({visible: true})
    }, 1000)

    setTimeout(() => {
      this.setState({visible: false})
    }, 5000)
  }

  _onValueChange = value => {
    this.setState({value})
  }

  render() {
    const {visible, value} = this.state;
    return (
      <View style={{flex: 1}}>
        <Modal visible={visible} animationType='fade'>
          <View style={{flex: 1, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center'}}>
          <Switch value={value} onValueChange={this._onValueChange} />
          </View>
        </Modal>
      </View>
    );
  }
}
