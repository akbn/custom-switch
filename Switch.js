import React, { Component } from 'react';
import { Animated, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

export default class Switch extends Component {
  static propTypes = {
    value: PropTypes.bool.isRequired,
    onValueChange: PropTypes.func.isRequired,
    color: PropTypes.string,
  }

  static defaultProps = {
    color: 'rgba(255,255,255,0.8)',
  }

  constructor(props) {
    super(props);
    this.state = {
      translateX: new Animated.Value(props.value ? 20 : 0),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this._toggle();
    }
  }

  _toggle = () => {
    const { translateX } = this.state;
    const { value } = this.props;
    Animated.spring(translateX, {
      toValue: value ? 0 : 20,
      friction: 5,
      tension: 40,
      useNativeDriver: true,
    }).start();
  }

  _onPress = () => {
    const { value, onValueChange } = this.props;
    this._toggle();
    onValueChange(!value);
  }

  render() {
    const { translateX } = this.state;
    const { value, color } = this.props;
    return (
      <TouchableOpacity onPress={this._onPress} style={{ borderWidth: 1, borderColor: color, width: 45, height: 25, borderRadius: 20, justifyContent: 'center' }} activeOpacity={0.7}><Animated.View style={{ backgroundColor: color, width: 20, height: 20, borderRadius: 25, marginHorizontal: 1, transform: [{ translateX }] }} useNativeDriver /></TouchableOpacity>
    );
  }
}
